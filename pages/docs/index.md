# UCL IT Sikkerhed projekt efterår 2022

De studerende på IT Sikkerhed skal løbende i dette semester udarbejde et tværfagligt projekt som skal dække læringsmålene for tre af fagene i dette semester.

Dette projekt danner grundlag for eksaminationer i fagene _Introduktion til IT Sikkerhed_,  
_Software sikkerhed_ og _IT sikkerhed i webapplikation_.ss

**Andre relevante IT sikkerheds fag må frit indrages i projektet.**

**_Det er meget vigtigt at i gennemlæser denne beskrivelse, og hurtigst muligt søger afklaring på evt. tvivlsspørgsmål hos en af de ansvarshavende undervisere._**


## Praktisk information

**Start dato:** Ved udlevering af denne beskrivelse.

**Afleverings dato:** Projektet og det udarbejdet materiale skal være tilgængeligt inden eksaminationen i hvert enkeltfag.
og jvf. underviseren i hvert af de tre fags henvisninger.

**Ansvarlige undervisere:** Nikolaj Esbjørn Simonsen & Martin Edwin Schjødt Nielsen.

**Projekt arbejdsdage:** Se skema. 

**Links til Studieordninger:**  
[Den institutionelle](https://esdhweb.ucl.dk/D22-1972441.pdf?nocache=7a985bee-1f60-4456-bf64-cbc56e4ad527&_ga=2.139969907.1586846701.1661414902-845159209.1651822878)  
[Den nationale](https://esdhweb.ucl.dk/D22-1980440.pdf?nocache=dc8fd5b5-a669-4507-8b60-b78047427e91&_ga=2.102542913.1586846701.1661414902-845159209.1651822878)


## Beskrivelse

Projektets udgangspunkt er en SOME [app](https://github.com/Silverbaq/ITSec-ImageSharing). Der skal i projektetet arbejdes indenfor de rammer som opstilles af de tre specificeret fag i dette semester. 

Følgende er et kort eksempel:

_Ole, Ahmad og Line har fagene Web applikation sikkerhed, Software sikkerhed og intro til IT-sikkerhed. Deres projekt er at udarbejde en webshop._  

_Alle tre er bevidste om at alle læringsmålene for de tre fags ikke kan dækkes, og de vælger derfor at fokusere på de mest væsentlige læringsmål i deres projekt._  

_Efter projekts start har teamet udarbejdet diverse artefakter samt en projekt beskrivelse.  
Ud fra projekt beskrivelsen har teamet fået godkendt deres projekt ide hos Jens Jørgen som er den ansvarshavende underviser på semester projektet_

_Til webshoppens frontend udvikler de en hjemmeside med meget fokus på at imødegå cross-site scripting._  
_De udvikler også en backend i .Net og har meget fokus på modellering, semantik, input validering og test._  

_Udover det foretager de en risikoanalyse af systemet og dokumenterer det generelle trusselsbillede._  

_Sidst men ikke mindst udarbejder teamet nogle Bash scripts som benyttes til automatisering af penetration tests._  
_Områderne som de gennem hele projektet fokuserer på, er de områder som passer med projektes domæne og undervisningens læringsmål._  

_Teamet arbejder agilt, da projektet kører sideløbende med den øvrige undervisning. Teamet dokumenterer deres arbejdsproces samt artefakter i et offentligt tilgængeligt gitlab projekt._ 

_Teamet deltager i løbet af projektet i planlagte milepæls aktiviteter (Milestones). Som eksempel, præsenterer teamet deres foreløbig arbejde for resten af klassen 3 uger efter projektstart hvilket er en milepæls aktivitet._

_Alle tre team medlemmer har læst studieordningen for uddannelsen grundigt._  
_Dels fordi de skal vise forståelse for læringsmålene i projektet. Men også fordi de skal være opmærksomme på hvordan de eksamineres i hver af de tre fag._  

_Eksempelvis kan de se i den institutionelle studieordning at eksamen i “software sikkerhed” er aflevering af en skriftlig rapport samt mundtlig eksamination. Forventningerne til rapporten får de fra deres underviser i faget._


**_Eksemplet er et tænkt scenarie og skal derfor ikke bruges som grundlag for fokus områder i projektet._**

## Krav til projektet

- Projektet skal dække væsentlige læringsmål fra hvert fag (Se studieordningen).
- Alle teams skal deltage i planlagte milepæls aktiviteter.
- Alt kode, dokumentation o.l. skal løbende dokumenteres i et offentlig gitlab projekt, placeret i gruppen [https://gitlab.com/22e-its-project-group](https://gitlab.com/22e-its-project-group) 
- Projektet skal indeholde udvikling af et teknisk produkt
- Projektet skal inkludere sikkerheds test af det tekniske produkt
- Projektet skal godkendes af de ansvarhavende underviserer.(Se milepæls aktiviteter)

## Eksamination

For hvert af de 3 fag er der en prøve jvf. afsnit 5.2 i den [instituitionelle del af studioordningen](https://esdhweb.ucl.dk/D22-1972441.pdf). 

Hvis et semester eksempelvis består af følgende fag:  

- Introduktion til IT-sikkerhed 
- Software sikkerhed
- Sikkerhed i web applikationer

Hvert af de 3 fag vil skulle afsluttes med en prøve.  
Prøveformen for hvert fag er beskrevet i den institutionelle studieordning.  

Eksempelvis kan et fags prøveform være beskrevet således:    

_Prøven er en individuel, mundtlig prøve med udgangspunkt i et spørgsmål, som den
studerende trækker til eksamen.  
Alle spørgsmål, der kan trækkes til eksamen, er udleveret til de studerende senest 14
dage før eksamen, så de studerende har mulighed for at forberede sig.  
**Hvad angår spørgsmålene, vil der blive lagt vægt på, at den studerende kan inddrage eksempler fra projektarbejdet og praktiske øvelser fra det forgangne semester.**  
Der er ingen forberedelse på selve dagen._      

I dette konkrete tilfælde vil det betyde at de eksempler som inddrages i eksamination er taget fra semester projektet.  
Et andet tilfælde kunne være at der inden den mundtlige eksamination skal afleveres en rapport.  
Her vil rapport skulle tage udgangspunkt i dele af projektet.

**Husk:**  
- Altid orienterer dig om den enkelte eksamen i den institutionelle studieordning.  
- Altid rådføre dig med underviseren i det enkelte fag ved tvivlsspørgsmål.  


## Milepæls aktiviteter (milestones)

Milepæls aktiviteter har til formål at støtte de studerende i løbet af deres projekt proces, samt at give underviserne indblik i de studerendes arbejde med projektet. 
På den måde skabe grundlag for vejledning og støtte.

**Alle milepæls aktiviteterne er obligatoriske for alle teams.** 

Figuren herunder viser en oversigt over milepæls aktiviteterne i projektet.

![milepæle_20220823.png](milepæle_20220823.png)

Milepæls aktiviteterne **må ikke misforstås som en opfordring til at arbejde ud fra en waterfall metodologi.**  
Vi opfordrer til at arbejde agilt (særligt når undervisningen foregår parallelt med projektet). Så der må gerne ske ændringer i projektet efter en milepæls aktivitet.  

I de følgende underafsnit er der en forklaring på hver milepæls aktivitet.

### 1. Præsentation af use cases og foreløbig projekt arbejde for klassen

I denne aktivitet skal teamet præsenterer de use cases som er udarbejdet til projektet. 

En use case viser den ønskede funktionalitet af projekts primære produkt og skaber derfor et godt overblik over hvad der skal laves i løbet af projektet.  
I præsentationen skaber det et godt udgangspunkt, for at vise hvad teamet har planlagt at lave.  
Endvidere skal teamet præsentere det øvrige arbejde der indtil videre er udført i projektet.

### 2. Godkendelse af projekt
For at underviserne kan yde bedst mulige vejledning og støtte gennem projektet, skal alle projekter godkendes af
en ansvarshavende underviser inden deadline. Der bør som minnimum forelægge en projekt beskrivelse og evt. enkelte artefakter.

### 3. Demo af nuværende produkt samt artefakter

I denne aktivitet skal teamet lave en demonstration af deres nuværende produkt samt alle udarbejdede artefakter (Modeller, diagrammer, analyser osv.)

### 4. Gruppe peer review session af artefakter og produkt

I denne aktivitet skal teams peer reviewe hinandens arbejde.  

Det vil sige at team x giver team y adgang til artefakter, dokumentation og produkt.  Team y giver ligeledes adgang til deres materiale.  
Team y og x gennemgår hver især det overdragede materiale, og noterer kommentar med henblik på feedback (jf. Feedback kriterier herunder)  

Når begge teams er færdige, gives på skift mundtlig feedback.   

Feedback kriterierne er som følger:

- Er der sammenhæng mellem artefakter, produkt og læringsmål?
- Giver dokumentationen mening ift. produktet?
- Giver artefakterne mening ift. produktet?

### 5. Præsentation af endeligt produkt, artefakter og dokumentation

I denne aktivitet skal teams præsentere alt de har produceret i projekt arbejdet på klassen.
Klassen vil efterfølgende havde mulighed for at stille spørgsmål.

Obs! Der må stadig godt laves justeringer inden den endelige aflevering aflevering.



